package com.example.android_learning_app.interfaces

import com.example.registrationformapplication.model.Student

interface OnItemClickListener {
    fun onItemClick(position: Int, student: Student)
}