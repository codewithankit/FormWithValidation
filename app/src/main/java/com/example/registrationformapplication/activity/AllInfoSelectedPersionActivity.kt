package com.example.registrationformapplication.activity

import android.annotation.SuppressLint
import android.app.ActionBar
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Base64
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isEmpty
import androidx.databinding.DataBindingUtil
import com.example.registrationformapplication.R
import com.example.registrationformapplication.databinding.ActivityAllInfoSelectedPersionBinding
import com.example.registrationformapplication.databinding.LayoutCustomBinding
import com.example.registrationformapplication.model.Student
import com.example.registrationformapplication.repository.SqliteDBRepository
import com.example.registrationformapplication.utility.Keys
import com.example.registrationformapplication.viewModal.FormActivityViewModel
import com.google.gson.Gson

@Suppress("EqualsBetweenInconvertibleTypes")
class AllInfoSelectedPersionActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding:ActivityAllInfoSelectedPersionBinding
    private lateinit var viewModel:FormActivityViewModel
    private lateinit var studentSingleRecord:Student
    private var dataListViewFormActivity : DataListViewFormActivity=DataListViewFormActivity()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_all_info_selected_persion)
        viewModel=FormActivityViewModel(SqliteDBRepository(this),this)

        val tempIntent=intent
        studentSingleRecord=Gson().fromJson(tempIntent.getStringExtra(Keys.SINGLESTUDENTRECORD),Student::class.java)
        binding.shapeableImageView.setImageBitmap(convertBase64ToImage(studentSingleRecord.photo))
        binding.showName.text="${studentSingleRecord.fName }${ studentSingleRecord.lName}"
        binding.showPhoneNumber.text=studentSingleRecord.phone
        binding.showAltPhoneNumber.text=studentSingleRecord.altphone
        binding.showEmail.text=studentSingleRecord.email
        binding.showAddress.text=studentSingleRecord.address
        binding.showDob.text=studentSingleRecord.dob
        binding.showGender.text=studentSingleRecord.gender
        binding.showCourse.text=
            studentSingleRecord.qualification.replace("[","",false).replace("]","",false)
        this.binding.imgCallBtn.setOnClickListener(this)

    }
    private fun convertBase64ToImage(base64Image : String) : Bitmap? {
        val decodedString : ByteArray = Base64.decode(base64Image, Base64.NO_WRAP)
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
    }




    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.context_menu_on_long_press, menu)
        return super.onCreateOptionsMenu(menu)
    }

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onOptionsItemSelected(item : MenuItem) : Boolean {
        when (item.itemId) {
            R.id.update_option -> {
                val layoutCustomBinding = LayoutCustomBinding.inflate(layoutInflater)
                val dialog = Dialog(this)
                dialog.setContentView(layoutCustomBinding.root)
                dialog.setCancelable(false)
                val windowManager = WindowManager.LayoutParams()
                windowManager.width = ActionBar.LayoutParams.MATCH_PARENT
                windowManager.height = ActionBar.LayoutParams.WRAP_CONTENT
                dialog.window?.attributes = windowManager
                dialog.show()
                layoutCustomBinding.tilFname.editText?.setText(studentSingleRecord.fName)
                layoutCustomBinding.tilLname.editText?.setText(studentSingleRecord.lName)
                layoutCustomBinding.tilPhone.editText?.setText(studentSingleRecord.phone)
                if (layoutCustomBinding.tilFname.isEmpty()){
                    layoutCustomBinding.etFirstNameUpdate.error=" First Name Can't be Empty "
                }else if (layoutCustomBinding.tilLname.isEmpty()){
                    layoutCustomBinding.etLastNameUpdate.error=" Last Name Can't be Empty "
                }else if(layoutCustomBinding.tilPhone.isEmpty()){
                    layoutCustomBinding.etPhoneUpdate.error=" Phone Number Can't be Empty "
                }else{
                    layoutCustomBinding.btnSubmit.setOnClickListener {
                        if (layoutCustomBinding.tilPhone.counterMaxLength!=10){
                            layoutCustomBinding.etPhoneUpdate.error=" Invalid Phone Number "
                        }
                        if (viewModel.updateSingleStudentRecord(
                                layoutCustomBinding.tilFname.editText?.text.toString(),
                                layoutCustomBinding.tilLname.editText?.text.toString(),
                                layoutCustomBinding.tilPhone.editText?.text.toString(),
                                studentSingleRecord.srNo.toInt())) {
                            dialog.dismiss()
                            dataListViewFormActivity.setRecyclerView()
                        } else {
                            Toast.makeText(this, " Something! went Wrong ", Toast.LENGTH_SHORT).show()
                        }

                    }
                }
            }

            R.id.delete_option -> {
                if (viewModel.deleteSingleStudentRecord(studentSingleRecord.srNo.toInt())) {
                    dataListViewFormActivity.setRecyclerView()
                    Toast.makeText(this, "Delete Successfully", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, "Something! went Wrong", Toast.LENGTH_SHORT).show()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v : View?) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) == PERMISSION_GRANTED){
            val intent = Intent(Intent.ACTION_CALL,Uri.parse("tel:${studentSingleRecord.phone}"))
            startActivity(intent)
        }else{
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.CALL_PHONE), 1001)
        }
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1001){
            if(grantResults.isNotEmpty() && permissions[0].equals(PERMISSION_GRANTED)
            ){
                Toast.makeText(this, " Permission Okay ", Toast.LENGTH_SHORT).show()
            }

        }
    }
}