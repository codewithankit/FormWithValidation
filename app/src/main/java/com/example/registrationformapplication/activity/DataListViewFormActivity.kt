package com.example.registrationformapplication.activity

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.android_learning_app.interfaces.OnItemClickListener
import com.example.registrationformapplication.R
import com.example.registrationformapplication.adapter.RecyclerViewAdapter
import com.example.registrationformapplication.databinding.ActivityDataListViewFormBinding
import com.example.registrationformapplication.factory.FormActivityViewModelFactory
import com.example.registrationformapplication.model.Student
import com.example.registrationformapplication.repository.SqliteDBRepository
import com.example.registrationformapplication.viewModal.FormActivityViewModel

class DataListViewFormActivity : AppCompatActivity(), OnItemClickListener {
    private lateinit var binding : ActivityDataListViewFormBinding
    private lateinit var viewModel:FormActivityViewModel
    private lateinit var factory:FormActivityViewModelFactory
    private lateinit var myAdapter:RecyclerViewAdapter
    var context:Context=this

    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_data_list_view_form)
        factory=FormActivityViewModelFactory(SqliteDBRepository(this),context)
        viewModel = ViewModelProvider(this@DataListViewFormActivity,factory)[FormActivityViewModel::class.java]
        setRecyclerView()
        myAdapter.setOnItemClickListener(context as DataListViewFormActivity)
    }

    override fun onCreateOptionsMenu(menu : Menu?) : Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.option_menu_delete, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item : MenuItem) : Boolean {
        when(item.itemId){
            R.id.delete_all->{
                if (viewModel.deleteAllRecord()){
                    setRecyclerView()
                    Toast.makeText(this, " All Records Delete Successfully ", Toast.LENGTH_SHORT).show()
                }

            }
        }
        return super.onOptionsItemSelected(item)
    }

    @SuppressLint("NotifyDataSetChanged", "SetTextI18n")
    fun setRecyclerView() {
        LinearLayoutManager(this).also { linearLayoutManager -> linearLayoutManager.also { this.binding.recyclerView.layoutManager = it } }
        val dataGetAll=viewModel.getAllData()
        if (dataGetAll.isEmpty()
        ){
          binding.tvDatabaseMassage.text=" No More Data Found! "
        }
        RecyclerViewAdapter(dataGetAll,context).also { myAdapter = it }
        myAdapter.also { binding.recyclerView.adapter = it }
        myAdapter.run { notifyDataSetChanged() }
    }

    override fun onItemClick(position : Int, student : Student) {


    }

}