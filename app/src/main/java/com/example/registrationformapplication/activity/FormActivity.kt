package com.example.registrationformapplication.activity

import android.annotation.SuppressLint
import android.app.ActionBar
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.view.*
import android.widget.CompoundButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.registrationformapplication.R
import com.example.registrationformapplication.databinding.ActivityFormBinding
import com.example.registrationformapplication.databinding.LayoutRateUsActivityBinding
import com.example.registrationformapplication.factory.FormActivityViewModelFactory
import com.example.registrationformapplication.obserable.FormFieldObserable
import com.example.registrationformapplication.repository.SqliteDBRepository
import com.example.registrationformapplication.utility.Keys
import com.example.registrationformapplication.viewModal.FormActivityViewModel
import com.google.android.material.datepicker.MaterialDatePicker
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*


@Suppress("DEPRECATION")
class FormActivity : AppCompatActivity(), View.OnClickListener, RadioGroup.OnCheckedChangeListener,
    CompoundButton.OnCheckedChangeListener {

    //----------------------------------------------------------------------------------------------
    private lateinit var binding : ActivityFormBinding
    private lateinit var viewModel : FormActivityViewModel
    private lateinit var factory: FormActivityViewModelFactory
    private lateinit var formfieldobserver : FormFieldObserable
    private var gender : String = ""
    private var arrayList = ArrayList<String>()
    private var base64Image:String=""


    //----------------------------------------------------------------------------------------------
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_form)
        factory=FormActivityViewModelFactory(SqliteDBRepository(this),this)
        viewModel = ViewModelProvider(this, factory)[FormActivityViewModel::class.java]


        //------------------------------------------------------------------------------------------
        formfieldobserver = FormFieldObserable()


        //------------------------------------------------------------------------------------------
        binding.myviewModaldata = viewModel



        //------------------------------------------------------------------------------------------
        binding.lifecycleOwner = this


        //------------------------------------------------------------------------------------------
        binding.btnsubmit.setOnClickListener(this)
        binding.rbGroup.setOnCheckedChangeListener(this)
        binding.cbKotlin.setOnCheckedChangeListener(this)
        binding.cbJava.setOnCheckedChangeListener(this)
        binding.cbFlutter.setOnCheckedChangeListener(this)
        binding.cbIos.setOnCheckedChangeListener(this)
        binding.cbPhp.setOnCheckedChangeListener(this)
        binding.cbNet.setOnCheckedChangeListener(this)
        binding.cbJavscript.setOnCheckedChangeListener(this)
        binding.cbCSharp.setOnCheckedChangeListener(this)
        binding.etDob.setOnClickListener(this)
        binding.fabCamera.setOnClickListener {
            actionFabButton()
        }



        //------------------------------------------------------------------------------------------
        val intent = intent
        val usertype = intent?.getStringExtra(Keys.USER_TYPE).toString()
        binding.usertypeplace.text = usertype
        binding.tvSystemdate.text = getCurrentDate()
        //------------------------------------End of OnCreate Function----------------------------------
    }

    private fun actionFabButton() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent,1002)
        }else{
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.CAMERA), 1002)
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode : Int, resultCode : Int, data : Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==1002 && resultCode== RESULT_OK){
            val bitmap:Bitmap= data?.extras?.get("data") as Bitmap
            binding.shapeableImageView.setImageBitmap(bitmap)

            val byteArrayOutputStream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
            val byteArray = byteArrayOutputStream.toByteArray()
            base64Image = Base64.encodeToString(byteArray,Base64.DEFAULT)
        }
    }

    //----------------------------------------------------------------------------------------------
    private fun getCurrentDate() : String {
        val date = Calendar.getInstance().time
        val formatter = SimpleDateFormat.getDateTimeInstance()
        return formatter.format(date).toString()
    }

    //----------------------------------------------------------------------------------------------
    override fun onClick(v : View?) {
       val getResult=viewModel.datavalidation(
           gender = gender,
           arrayList = arrayList,
           base64Image = base64Image
       )
       if (getResult==1)
       {
           Toast.makeText(this, " Submit Successfully ", Toast.LENGTH_SHORT).show()
           binding.etFirstname.editText?.text!!.clear()
           binding.etLastname.editText?.text!!.clear()
           binding.etMobilenumber.editText?.text!!.clear()
           binding.etAltmob.editText?.text!!.clear()
           binding.etDob.editText?.text!!.clear()
           binding.etEmail.editText?.text!!.clear()
           binding.etAddress.editText?.text!!.clear()
           binding.cbCSharp.isChecked=false
           binding.cbFlutter.isChecked=false
           binding.cbIos.isChecked=false
           binding.cbJava.isChecked=false
           binding.cbJavscript.isChecked=false
           binding.cbKotlin.isChecked=false
           binding.cbNet.isChecked=false
           binding.cbPhp.isChecked=false
           val drawable = ContextCompat.getDrawable(this, R.drawable.baseline_account_circle_24)
           base64Image=""
           binding.shapeableImageView.setImageDrawable(drawable)
       }else{
           when(getResult){
               2-> binding.etFirst.error="First Name Can't be Empty"
               3-> binding.etLast.error = "Last Name Can't be Empty"
               4-> binding.etMobile.error="Invalid Mobile Number"
               5-> binding.etAltphone.error="Invalid AltMobile Number"
               8-> binding.etAdd.error="Invalid Address"
               6-> binding.etMail.error="Invalid Email"
               9-> Toast.makeText(this, "Please Select Gender", Toast.LENGTH_SHORT).show()
               10-> Toast.makeText(this, "Please Select Qualification", Toast.LENGTH_SHORT).show()
               11-> Toast.makeText(this, "Please Select Picture", Toast.LENGTH_SHORT).show()
               12-> binding.etAltphone.error="Enter Different Mobile Number"

           }
       }
    }


    //--------------------------------------- This Event of Redio Buttons --------------------------
    override fun onCheckedChanged(group : RadioGroup?, checkedId : Int) {
        when (group?.checkedRadioButtonId) {
            R.id.rb_male -> {

                gender = binding.rbMale.text.toString()
            }

            R.id.rb_female -> {

                gender = binding.rbFemale.text.toString()
            }

            R.id.rb_custom -> {
                gender = binding.rbCustom.text.toString()
            }

        }
    }
    //------------------------------ End of Redio Button Event Block -------------------------------


    // Here I Use Code for disable back button Action
    //----------------------------------------------------------------------------------------------
    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        showAlertDialog()
    }
    //----------------------------------------------------------------------------------------------
    private fun showAlertDialog() {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setTitle("Confirmation")
        alertDialog.setMessage(" Do You Want to Exit ")
        alertDialog.setPositiveButton(
            "Yes"
        ) { _, _ -> finish() }

        alertDialog.setNegativeButton(
            "No"
        ) { _, _ -> }

        val alert : AlertDialog = alertDialog.create()
        alert.show()
    }
    //----------------------------------------------------------------------------------------------



    //------------------------------- This Event of MenuBar ----------------------------------------
    override fun onCreateOptionsMenu(menu : Menu?) : Boolean {
        val inflater : MenuInflater = menuInflater
        inflater.inflate(R.menu.manubars, menu)
        return super.onCreateOptionsMenu(menu)
    }
    //-------------------------------- End of MenuBar Event Block ----------------------------------


    //-------------------- This Event of MenuBar Which will be Action Perform ----------------------
    override fun onOptionsItemSelected(item : MenuItem) : Boolean {
        when (item.itemId) {

            R.id.viewform -> {
              startActivity(Intent(this,DataListViewFormActivity::class.java))
            }

            R.id.logout -> {
                val alertDialog = AlertDialog.Builder(this)
                alertDialog.setTitle("Confirmation")
                alertDialog.setMessage(" Do You Want to LogOut ")
                alertDialog.setPositiveButton(
                    "Yes"
                ) { _, _ -> startActivity(Intent(this,LoginActivity::class.java)) }

                alertDialog.setNegativeButton(
                    "No"
                ) { _, _ -> }

                val alert : AlertDialog = alertDialog.create()
                alert.show()
            }

            R.id.rate_us -> {
                val layoutRateUsActivityBinding =
                    LayoutRateUsActivityBinding.inflate(layoutInflater)
                showRatingBar(layoutRateUsActivityBinding)
            }

        }
        return super.onOptionsItemSelected(item)
    }

    private fun showRatingBar(layoutRateUsActivityBinding : LayoutRateUsActivityBinding) {
        val dialog = Dialog(this)
        dialog.setContentView(layoutRateUsActivityBinding.root)
        dialog.setCancelable(false)
        val windowManager = WindowManager.LayoutParams()
        windowManager.width = ActionBar.LayoutParams.MATCH_PARENT
        windowManager.height = ActionBar.LayoutParams.WRAP_CONTENT
        dialog.window?.attributes = windowManager
        dialog.show()
        layoutRateUsActivityBinding.ratingbar.setOnRatingBarChangeListener { ratingBar, _, _ ->
            layoutRateUsActivityBinding.btnSubmit.setOnClickListener {
                Toast.makeText(this, "Submit Successfully ${ratingBar.rating}", Toast.LENGTH_SHORT)
                    .show()
                dialog.dismiss()
            }
        }

    }
    //------------------------- End of MenuBar Event Block -----------------------------------------


    //--------------------------- This Event of CheckBoxes -----------------------------------------
    @SuppressLint("SimpleDateFormat")
    override fun onCheckedChanged(buttonView : CompoundButton?, isChecked : Boolean) {
        when (buttonView?.id) {
            R.id.cb_kotlin -> {
                if (binding.cbKotlin.isChecked) {
                    arrayList.add(binding.cbKotlin.text.toString())

                } else {
                    arrayList.remove(binding.cbKotlin.text.toString())
                }

            }
            R.id.cb_java -> {
                if (binding.cbJava.isChecked) {
                    arrayList.add(binding.cbJava.text.toString())

                } else {
                    arrayList.remove(binding.cbJava.text.toString())

                }

            }
            R.id.cb_php -> {
                if (binding.cbPhp.isChecked) {
                    arrayList.add(binding.cbPhp.text.toString())

                } else {
                    arrayList.remove(binding.cbPhp.text.toString())

                }

            }
            R.id.cb_C_Sharp -> {
                if (binding.cbCSharp.isChecked) {
                    arrayList.add(binding.cbCSharp.text.toString())

                } else {
                    arrayList.remove(binding.cbCSharp.text.toString())


                }

            }
            R.id.cb__net -> {
                if (binding.cbNet.isChecked) {
                    arrayList.add(binding.cbNet.text.toString())

                } else {
                    arrayList.remove(binding.cbNet.text.toString())

                }

            }


            R.id.cb_ios -> {
                if (binding.cbIos.isChecked) {
                    arrayList.add(binding.cbIos.text.toString())

                } else {
                    arrayList.remove(binding.cbIos.text.toString())

                }

            }


            R.id.cb_flutter -> {
                if (binding.cbFlutter.isChecked) {
                    arrayList.add(binding.cbFlutter.text.toString())

                } else {
                    arrayList.remove(binding.cbFlutter.text.toString())

                }

            }


            R.id.cb_javscript -> {
                if (binding.cbJavscript.isChecked) {
                    arrayList.add(binding.cbJavscript.text.toString())

                } else {
                    arrayList.remove(binding.cbJavscript.text.toString())

                }

            }

            R.id.et_dob->{
                MaterialDatePicker.Builder.datePicker().build()
                    .show(supportFragmentManager, "DATE PICKER")
                val datePickerBuilder = MaterialDatePicker.Builder.datePicker()
                datePickerBuilder.setTitleText("SELECT UR DOB")
                val datePicker = datePickerBuilder.build()


                datePicker.show(supportFragmentManager, "DATE")
                datePicker.addOnPositiveButtonClickListener {
                    val calender = Calendar.getInstance()
                    calender.time = Date(it)
                    val day = calender.get(Calendar.DAY_OF_MONTH)
                    val month = calender.get(Calendar.MONTH) + 1
                    val year = calender.get(Calendar.YEAR)

                    Toast.makeText(this, "$day / $month / $year", Toast.LENGTH_SHORT)
                        .show()
                    val simpelDateFormat = SimpleDateFormat("dd/MM/yyyy")
                    val selectedDate = simpelDateFormat.format(Date(it))
                    Toast.makeText(
                        this,
                        "You have selected: $selectedDate",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

        }
    }
    //------------------------------------------- End of CheckBoxes Event Block---------------------
}
//--------------------------------End of Main Thread------------------------------------------------