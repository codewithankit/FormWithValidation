package com.example.registrationformapplication.repository

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.ContactsContract.CommonDataKinds.Photo.PHOTO
import com.example.registrationformapplication.model.Student


private const val DB_NAME = "_db_chetu"
private const val DB_VERSION = 1
private const val TABLE_NAME = "student"
private const val FNAME = "FirstName"
private const val LNAME = "LastName"
private const val PHONE = "Phone"
private const val ALTPHONE="AltPhoneNumber"
private const val DOB="Dob"
private const val EMAIL="Email"
private const val ADDRESS="Address"
private const val GENDER="Gender"
private const val LANGUAGES="Languages"
private const val PHOTO = "photo"
private const val SRNO = "Sr"


@Suppress("UNREACHABLE_CODE")
class SqliteDBRepository(context: Context) {

    val query =
        "CREATE TABLE $TABLE_NAME($SRNO INTEGER PRIMARY KEY AUTOINCREMENT, $FNAME TEXT, $LNAME TEXT, $PHONE TEXT, $ALTPHONE TEXT, $DOB TEXT, $EMAIL TEXT, $GENDER TEXT,$ADDRESS TEXT, $LANGUAGES TEXT , $PHOTO  TEXT )"
    private val dbHelper = MyDbHelper(context)
    private val sqliteDb = dbHelper.writableDatabase!!

    fun createData(fName: String, lName: String, phone: String,altphone:String,dob:String,email:String,gender:String,address:String,languages:String,photo:String):Boolean {
        val contentValue = ContentValues()
        contentValue.put(FNAME, fName)
        contentValue.put(LNAME, lName)
        contentValue.put(PHONE, phone)
        contentValue.put(ALTPHONE,altphone)
        contentValue.put(DOB,dob)
        contentValue.put(EMAIL,email)
        contentValue.put(GENDER,gender)
        contentValue.put(ADDRESS,address)
        contentValue.put(LANGUAGES,languages)
        contentValue.put(PHOTO,photo)
        val id: Long = sqliteDb.insert(TABLE_NAME, null, contentValue)
        return id > 0
    }

    @SuppressLint("Recycle")
    fun getAllData() : ArrayList<Student>{
        val listOfStudent = ArrayList<Student>()
        val columns = arrayOf(SRNO, FNAME, LNAME, PHONE, ALTPHONE, DOB, EMAIL, GENDER, ADDRESS,
            LANGUAGES,PHOTO)
        val cursor = sqliteDb.query(TABLE_NAME, columns, null, null, null, null, null)
        if (cursor.count > 0) {
            cursor.moveToFirst()
            do {
                val srNo = cursor.getString(0)
                val fName = cursor.getString(1)
                val lName = cursor.getString(2)
                val phone = cursor.getString(3)
                val altphone=cursor.getString(4)
                val dob=cursor.getString(5)
                val email=cursor.getString(6)
                val gender=cursor.getString(7)
                val address=cursor.getString(8)
                val languages=cursor.getString(9)
                val photo=cursor.getString(10)
                val student = Student(srNo, fName, lName, phone,altphone,dob,email,gender, address , languages,photo)

                listOfStudent.add(student)

            } while (cursor.moveToNext())
        }

        return listOfStudent
    }


    fun deleteSingleRecord(rowId : Int) : Boolean {
        val id = sqliteDb.delete(TABLE_NAME, "$SRNO=$rowId", null)
        return id > 0
    }

    fun updateSingleRecord(firstName:String,lastName:String,phoneNumber:String,rowId:Int):Boolean{
        val contentValue = ContentValues()
        contentValue.put(FNAME, firstName)
        contentValue.put(LNAME, lastName)
        contentValue.put(PHONE, phoneNumber)
        val id = sqliteDb.update(TABLE_NAME, contentValue, "$SRNO=$rowId", null)
        return id > 0

    }

    fun deleteAllRecords():Boolean{
        val id = sqliteDb.delete(TABLE_NAME, null, null)
        return id > 0
        sqliteDb
    }


    inner class MyDbHelper(context: Context) :
        SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {
        override fun onCreate(sqliteDb: SQLiteDatabase?) {
            sqliteDb?.execSQL(query)
        }

        override fun onUpgrade(sqliteDb: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

        }

    }
}