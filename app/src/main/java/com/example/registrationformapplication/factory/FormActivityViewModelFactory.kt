package com.example.registrationformapplication.factory

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.registrationformapplication.repository.SqliteDBRepository
import com.example.registrationformapplication.viewModal.FormActivityViewModel

@Suppress("UNCHECKED_CAST")
class FormActivityViewModelFactory(
    private val repository : SqliteDBRepository,private val context:Context
): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(FormActivityViewModel::class.java)){
            return FormActivityViewModel(repository,context) as T
        }
        throw IllegalArgumentException("Unknown class")
    }
}