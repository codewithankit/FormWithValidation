package com.example.registrationformapplication.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory.decodeByteArray
import android.util.Base64
import android.util.Base64.decode
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.android_learning_app.interfaces.OnItemClickListener
import com.example.registrationformapplication.R
import com.example.registrationformapplication.activity.AllInfoSelectedPersionActivity
import com.example.registrationformapplication.activity.DataListViewFormActivity
import com.example.registrationformapplication.databinding.LayoutItemsBinding
import com.example.registrationformapplication.model.Student
import com.example.registrationformapplication.utility.Keys
import com.google.gson.Gson


class RecyclerViewAdapter(private val listOfStudent :ArrayList<Student>, private val context : Context):RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>() {

    private lateinit  var onItemClickListener : OnItemClickListener

    inner class MyViewHolder(var binding : LayoutItemsBinding):RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent : ViewGroup, viewType : Int) : MyViewHolder {
        val view: View? =LayoutInflater.from(parent.context).inflate(R.layout.layout_items,parent,false)
        val binding: LayoutItemsBinding? =DataBindingUtil.bind(view!!)!!
        return binding?.let { MyViewHolder(it) }!!
    }

    override fun getItemCount() : Int {
       return listOfStudent.size
    }

    override fun onBindViewHolder(holder : MyViewHolder, position : Int) {
        val student = listOfStudent[position]
        holder.binding.apply {
            this.firstNameLayoutItem.text = student.fName
            this.lastNameLayoutItem.text = student.lName
            this.phoneNumberLayoutItem.text = student.phone
            student.fName
                 this.shapeableImageView.setImageBitmap(convertBase64ToImage(student.photo))
            this.imgButton.setOnClickListener {
                val intent=Intent(context,
                    AllInfoSelectedPersionActivity::class.java)
                val getStudentOne= listOfStudent[position]
                intent.putExtra(Keys.SINGLESTUDENTRECORD,Gson().toJson(getStudentOne))
                context.startActivity(intent)
            }
        }
    }
    fun setOnItemClickListener(onItemClickListener : DataListViewFormActivity){
        this.onItemClickListener = onItemClickListener
    }

    private fun convertBase64ToImage(base64Image : String) : Bitmap? {
        val decodedString : ByteArray = decode(base64Image, Base64.NO_WRAP)
        return decodeByteArray(decodedString, 0, decodedString.size)
    }

}