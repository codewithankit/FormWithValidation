package com.example.registrationformapplication.utility

class Keys {
    companion object{
        const val FNAME = "fName"
        const val LNAME = "lName"
        const val MOBNO = "mobNo"
        const val USER_TYPE="usertype"
        const val SINGLESTUDENTRECORD="singlestudentrecord"
    }
}