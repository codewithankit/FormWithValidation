package com.example.registrationformapplication.viewModal

import android.app.DatePickerDialog
import android.content.Context
import androidx.lifecycle.ViewModel
import com.example.registrationformapplication.model.Student
import com.example.registrationformapplication.obserable.FormFieldObserable
import com.example.registrationformapplication.repository.SqliteDBRepository
import java.util.Calendar

@Suppress("NAME_SHADOWING", "ControlFlowWithEmptyBody", "KotlinConstantConditions")
class FormActivityViewModel(
    private val repository : SqliteDBRepository,
    private val context : Context,
) : ViewModel() {
    //----------------------------------------------------------------------------------------------
    private var firstName:String=""
    private var lastName:String=""
    private var mobileNumber:String=""
    private var altMobileNumber:String=""
    private var email:String=""
    private var _gender:String=""
    private var dob:String=""
    private var address:String=""
    private lateinit var languages:ArrayList<String>
    private var returnError: Int=0

    //----------------------------------------------------------------------------------------------
    val formfieldobserable : FormFieldObserable = FormFieldObserable()


    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    fun datavalidation(
        gender : String,
        arrayList : ArrayList<String>,
        base64Image : String,
    ):Int {
        val condition : Boolean
        firstName= formfieldobserable.fnameform
        lastName= formfieldobserable.lnameform
        mobileNumber= formfieldobserable.mobilenumberform
        altMobileNumber= formfieldobserable.altnumberform
        address= formfieldobserable.addressform
        email= formfieldobserable.emailform
        dob=formfieldobserable.dobform
        this._gender = gender
        languages=arrayList
        if (isFirstNameValidate()){
        }else{
            returnError=2
            return this.returnError
        }
        if (isLastNameValidate()){
        }else{
            returnError=3
            return  returnError
        }
        if (isMobileNumberValid(mobileNumber)){
        }else{
            returnError=4
             return returnError
        }
        if (isAltMobileNumberValid(altMobileNumber)){
        }else{
            returnError=5
            return this.returnError
        }
        if (checkEmailValidation(email)){
        }else{
            returnError=6
            return returnError
        }
        if (isAddressvalidate()){
        }else{
            returnError=8
            return returnError
        }
        if (isGenderValidate()){
        }else{
            returnError=9
            return returnError
        }
        if (isLanguagesValidate()){
            condition=true
        }else{
            returnError=10
            return returnError
        }
        if (isValidateImage(base64Image)){

        }else{
            returnError=11
            return returnError
        }
        if (isSameMobileNumbers()){

        }else{
            returnError=12
            return  returnError
        }

        if (condition) {
            val qualification=languages.toString()

            repository.run {
                createData(
                        firstName,
                        lastName,
                        mobileNumber,
                        altMobileNumber,
                        dob,
                        email,
                        _gender,
                        address,
                       qualification,
                        base64Image
                    )
            }
            returnError = 1

        }
        return returnError
    }

    private fun isSameMobileNumbers() : Boolean {
        return this.mobileNumber != this.altMobileNumber
    }

    private fun isValidateImage(base64Image : String) : Boolean {
        return base64Image != ""

    }

    private fun isLanguagesValidate() : Boolean {
        return this.languages.size !=0
    }

    private fun isGenderValidate() : Boolean {
        return this._gender.isNotEmpty()
    }

    private fun isAddressvalidate() : Boolean {
        return this.address.isNotEmpty()
    }

    private fun isLastNameValidate() : Boolean {
     return this.lastName.isNotEmpty()
    }

    private fun isFirstNameValidate() : Boolean {
        return this.firstName.isNotEmpty()
    }


    private fun isMobileNumberValid(mobileNumber:String) : Boolean {
        return mobileNumber.isNotEmpty() && mobileNumber.length == 10
        }

    private fun isAltMobileNumberValid(altMobileNumber:String) : Boolean {
        return altMobileNumber.isNotEmpty() && altMobileNumber.length==10
    }

    private fun checkEmailValidation(email:String) : Boolean {
        val regex = Regex("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}")
        return if (regex.matches(email)){
            true
        }else{
            returnError=7
            false
        }
    }

    //----------------------------------------------------------------------------------------------
    fun onClickDob(){
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        // Create a DatePickerDialog to allow the user to select a date
        val datePickerDialog = DatePickerDialog(context, { _,year, month, dayOfMonth ->
            // Update the editText view with the selected date
            val selectedDate = "$dayOfMonth/${month+1}/$year"
            formfieldobserable.dobform=selectedDate
        }, year, month, day)

        // Show the DatePickerDialog
        datePickerDialog.show()
    }

    fun getAllData(): ArrayList<Student>{
       return repository.getAllData()
    }


    fun deleteSingleStudentRecord(rowId:Int):Boolean{
        return repository.deleteSingleRecord(rowId)
    }

    fun updateSingleStudentRecord(firstName:String,lastName:String,phoneNumber:String,rowId:Int):Boolean{
        return repository.updateSingleRecord(firstName,lastName,phoneNumber,rowId)
    }

    fun deleteAllRecord():Boolean{
        return repository.deleteAllRecords()
    }

}
//--------------------------------------------------------------------------------------------------